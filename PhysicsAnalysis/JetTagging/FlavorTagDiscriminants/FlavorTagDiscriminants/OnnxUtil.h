/*
  Copyright (C) 2002-2024 CERN for the benefit of the ATLAS collaboration

  This class acts as the interface to an ONNX model. It handles loading model
  the model, initializing the ORT session, and running inference. It is decoupled 
  from the ATLAS EDM as much as possible. The FlavorTagDiscriminants::GNN class
  handles the interaction with the ATLAS EDM.
*/

#ifndef ONNXUTIL_H
#define ONNXUTIL_H

#include <core/session/onnxruntime_cxx_api.h>

#include "nlohmann/json.hpp"
#include "lwtnn/parse_json.hh"

#include "FlavorTagDiscriminants/OnnxOutput.h"

#include <map> //also has std::pair
#include <vector>
#include <string>
#include <memory>

namespace FlavorTagDiscriminants {

  typedef std::pair<std::vector<float>, std::vector<int64_t>> input_pair;

  enum class OnnxModelVersion{UNKNOWN, V0, V1};

  NLOHMANN_JSON_SERIALIZE_ENUM( OnnxModelVersion , {
    { OnnxModelVersion::UNKNOWN, "" },
    { OnnxModelVersion::V0, "v0" },
    { OnnxModelVersion::V1, "v1" },
  })

  //
  // Utility class that loads the onnx model from the given path
  // and runs inference based on the user given inputs

  class OnnxUtil final{

    public:
      using OutputConfig = std::vector<OnnxOutput>;

      OnnxUtil(const std::string& path_to_onnx);

      void initialize();

      std::tuple<
        std::map<std::string, float>,
        std::map<std::string, std::vector<char>>,
        std::map<std::string, std::vector<float>> >
      runInference(
        std::map<std::string, input_pair> & gnn_inputs) const;

      const lwt::GraphConfig getLwtConfig() const;
      const nlohmann::json& getMetadata() const;
      const OutputConfig& getOutputConfig() const;
      OnnxModelVersion getOnnxModelVersion() const;

    private:
      nlohmann::json loadMetadata(const std::string& key) const;
      nlohmann::json m_metadata;

      std::string m_path_to_onnx;

      std::unique_ptr< Ort::Session > m_session;
      std::unique_ptr< Ort::Env > m_env;

      std::vector<std::string> m_input_node_names;
      OutputConfig m_output_nodes;

      OnnxModelVersion m_onnx_model_version = OnnxModelVersion::UNKNOWN;

  }; // Class OnnxUtil
} // end of FlavorTagDiscriminants namespace

#endif //ONNXUTIL_H
